import React, { useState, useEffect } from 'react'
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import { CFooter } from '@coreui/react';

const AddNews = ({...props}) => {
    const [title, setTitle] = useState('')
    const [category, setCategory] = useState('')
    const [content, setContent] = useState('')
   
    const savePosts  = async ()=>{
        const dataSave ={
            'title':title,
            'category':category,
            'content':content,
        }
    
    axios.post(`/api/posts/${id}`, dataSave)
    .then(response => {
        console.log(response); 
    })
} 

console.log("masih error");

    

   
    
    

    return (
        <div>
            <h1>Create Article</h1>
            <hr />
            <div className="row mb-1">
                <div className="col-mb-4">
                    <FormControl fullWidth>
                        <TextField
                            label="Title"
                            variant="outlined"
                            value={title}
                            onChange={(e)=> setTitle (e.target.value)}

                        />
                    </FormControl>
                </div>
                <div className="col-md-3">
                    <TextField
                        label="Content"
                        variant="outlined"
                        value={content}
                        onChange={(e)=> setContent (e.target.value)}

                    />
                </div>
                <div className="col-mb-4">
                    <TextField
                        label="Category"
                        variant="outlined"
                       value={category}
                       onChange={(e)=> setCategory (e.target.value)}

                    />
                </div>
                {/* <div className="col-md-3">
                    <TextField
                        label="Content"
                        variant="outlined"
                        onChange={handleName}

                    />
                </div> */}


                <CFooter>
                    <Button variant="outlined" color="primary" onClick={AddNews}>
                        Submit
                    </Button>
                </CFooter>
            </div>
        </div>
    )
}

export default AddNews;